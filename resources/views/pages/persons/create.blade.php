<h1>Créer la personne</h1>

{{Form::open(['route' => 'person.store'])}}
    <p>
        {{ Form::label('name', 'Nom :') }}
        {{ Form::text('name') }}
    </p>
    <p>
        {{ Form::label('height', 'Poid :') }}
        {{ Form::number('height') }}
    </p>
    <p>
        {{ Form::label('masse', 'Masse :') }}
        {{ Form::number('masse') }}
    </p>
    <p>
        {{ Form::label('gender', 'Genre :') }}
        {{ Form::text('gender') }}
    </p>
    <p>
        {{ Form::label('planet', 'Planète :') }}
        {{ Form::select('planet', $planets, null, ['placeholder' => 'Selectionne une planète'])}}
    </p>
    <p>
        {{ Form::label('specie', 'Espèce :') }}
        {{ Form::select('specie', $species, null, ['placeholder' => 'Selectionne une espèce']) }}
    </p>
    <p>
        {{Form::label('vehicles', 'Vehicules')}}
        {{Form::select('vehicles',$vehicles,null,['multiple'=>'multiple','name'=>'vehicles[]'])}}
    </p>
    <p>
        {{Form::label('starships', 'Vaisseaux')}}
        {{Form::select('starships',$starships,null,['multiple'=>'multiple','name'=>'starships[]'])}}
    </p>
    {{ Form::submit() }}