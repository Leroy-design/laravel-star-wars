<h1>Les personnes présentes </h1>
<div class="container">

{{Form::open(['route' => 'person.index', 'method' => 'GET'])}}
    <p>
        {{ Form::label('search', 'Rechercher :') }}
        {{ Form::text('search')}}
    </p>
    <p>
        {{ Form::label('planet', 'Planète :') }}
        {{ Form::select('planet', $planets, $planet, ['placeholder' => 'Selectionne une planète']) }}
    </p>
    <p>
        {{ Form::label('specie', 'Espèce :') }}
        {{ Form::select('specie', $species, $specie, ['placeholder' => 'Selectionne une espèce']) }}
    </p>
    <p>
        {{Form::label('vehicles', 'Vehicules')}}
        {{Form::select('vehicles',$vehicles, null, ['multiple'=>'multiple','name'=>'vehicles[]']) }}
    </p>
    <p>
        {{Form::label('starships', 'Vaisseaux')}}
        {{Form::select('starships',$starships, null, ['multiple'=>'multiple','name'=>'starships[]'])}}
    </p>
{{ Form::submit() }}
    
    <div class="row pt-5">
        <div class="col-4 pb-4">
            <table>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Poids</th>
                    <th>Masse</th>
                    <th>Genre</th>
                    <th>Espèce</th>
                    <th>Planet</th>
                    <th>Véhicules</th>
                    <th>Vaisseaux</th>
                    <th>Altération</th>
                </tr>
                @foreach($persons as $person)

                    <tr>
                        <th>{{$person->id}}</th>
                        <th>{{$person->name}}</th>
                        <th>{{$person->height}}</th>
                        <th>{{$person->masse}}</th>
                        <th>{{$person->gender}}</th>
                        <th>{{$person->specie->name}}</th>
                        <th>{{$person->planet->name}}</th>
                        <th>{{$person->vehicles->pluck('name')}}</th>
                        <th>{{$person->starships->pluck('name')}}</th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
