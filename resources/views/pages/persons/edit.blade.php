<h1>Editer la personne</h1>

{{ Form::model($person, ['route' => ['person.update', $person->id], 'method' => 'put'])}}
    <p>
        {{ Form::label('name', 'Nom :') }}
        {{ Form::text('name') }}
    </p>
    <p>
        {{ Form::label('height', 'Poid :') }}
        {{ Form::number('height') }}
    </p>
    <p>
        {{ Form::label('masse', 'Masse :') }}
        {{ Form::number('masse') }}
    </p>
    <p>
        {{ Form::label('gender', 'Genre :') }}
        {{ Form::text('gender') }}
    </p>
    <p>
        {{ Form::label('planet', 'Planète :') }}
        {{ Form::select('planet', $planets, $person->planet->id) }}
    </p>
    <p>
        {{ Form::label('specie', 'Espèce :') }}
        {{ Form::select('specie', $species, $person->specie->id) }}
    </p>
    <p>
        {{Form::label('vehicles', 'Vehicules')}}
        {{Form::select('vehicles',$vehicles,$person->vehicles->pluck('id')->toArray(),['multiple'=>'multiple','name'=>'vehicles[]'])}}
    </p>
    <p>
        {{Form::label('starships', 'Vaisseaux')}}
        {{Form::select('starships',$starships,$person->starships->pluck('id')->toArray(),['multiple'=>'multiple','name'=>'starships[]'])}}
    </p>
    {{ Form::submit() }}
{{ Form::close() }}