<h1>Les véhicules  </h1>
<div class="container">

    <div class="row pt-5">
        <div class="col-4 pb-4">
            <table>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Longueur</th>
                    <th>Conducteur</th>
                </tr>
                @foreach($vehicles as $vehicle)

                    <tr>
                        <th>{{$vehicle->id}}</th>
                        <th>{{$vehicle->name}}</th>
                        <th>{{$vehicle->length}}</th>
                        <th>{{$vehicle->persons->pluck('name')}}</th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

