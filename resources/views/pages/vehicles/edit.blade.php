<h1>Editer un véhicule</h1>

{{ Form::model($vehicle, [ 'url' => URL::action('VehicleController@update', $vehicle), 'method' => 'put'])}}
<p>{{ Form::label('name', 'Nom :') }} {{ Form::text('name') }}</p>
<p>{{ Form::label('length', 'Longeur :') }} {{ Form::number('length') }}</p>
{{ Form::submit() }}
{{ Form::close() }}
