<h1>Création d'un véhicule</h1>

{{ Form::open([
    'route' => 'vehicle.store',
    'method' => 'POST'
])}}
<div>
    {{ Form::label('name', 'Nom :')}}
    {{ Form::text('name') }}
</div>
<div>
    {{ Form::label('length', 'Longeur (en mètre):') }}
    {{ Form::number('length')}}
</div>
{{ Form::submit() }}

