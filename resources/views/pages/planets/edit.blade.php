

<h1>Editer une planète</h1>

{{ Form::model($planet, [ 'url' => URL::action('PlanetController@update', $planet), 'method' => 'put'])}}
<p>{{ Form::label('name', 'Nom :') }} {{ Form::text('name') }}</p>
<p>{{ Form::label('diameter', 'Diamètre :') }} {{ Form::number('diameter') }}</p>
<p>{{ Form::label('climate', 'Climat :') }} {{ Form::text('climate') }}</p>
<p>{{ Form::label('popuation', 'Population :') }} {{ Form::text('population') }}</p>
{{ Form::submit() }}
{{ Form::close() }}
