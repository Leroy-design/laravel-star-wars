
<h1>Les planètes présentes </h1>
<div class="container">

    <div class="row pt-5">
        <div class="col-4 pb-4">
            <table>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Diamètre</th>
                    <th>Climat</th>
                    <th>Population</th>
                    <th>Habitant</th>

                </tr>
                @foreach($planets as $planet)

                <tr>
                    <th>{{$planet->id}}</th>
                    <th>{{$planet->name}}</th>
                    <th>{{$planet->diameter}}</th>
                    <th>{{$planet->climate}}</th>
                    <th>{{$planet->population}}</th>
                    <th>{{$planet->persons->pluck('name')}}</th>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
