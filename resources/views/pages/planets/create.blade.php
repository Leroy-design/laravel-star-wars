{{-- @extends('default'); --}}
<h1>Créer une planète</h1>

{{ Form::open( [
    'route' =>'planet.store'
])}}
        <p>{{ Form::label('name', 'Nom :') }} {{ Form::text('name') }}</p>
        <p>{{ Form::label('diameter', 'Diamètre :') }} {{ Form::number('diameter') }}</p>
        <p>{{ Form::label('climate', 'Climat :') }} {{ Form::text('climate') }}</p>
        <p>{{ Form::label('popuation', 'Population :') }} {{ Form::text('population') }}</p>

{{ Form::submit() }}


