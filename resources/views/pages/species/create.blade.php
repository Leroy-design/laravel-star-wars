<h1>Création d'une espèce</h1>

{{ Form::open([
    'route' => 'specie.store',
    'method' => 'POST'
])}}
    <div>
        {{ Form::label('name', 'Nom :')}}
        {{ Form::text('name') }}
    </div>
    <div>
        {{ Form::label('classification', 'classification') }}
        {{ Form::text('classification') }}
    </div>
{{ Form::submit() }}
