

<h1>Les espèces en présence </h1>
<div class="container">

    <div class="row pt-5">
        <div class="col-4 pb-4">
            <table>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Classification</th>
                    <th>Représentant</th>
                </tr>
                @foreach($species as $specie)

                    <tr>
                        <th>{{$specie->id}}</th>
                        <th>{{$specie->name}}</th>
                        <th>{{$specie->classification}}</th>
                        <th>{{$specie->persons->pluck('name')}}</th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

