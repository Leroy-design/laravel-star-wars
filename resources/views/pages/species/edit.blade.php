<h1>Editer une espèce</h1>

{{ Form::model($specie, [ 'url' => URL::action('SpecieController@update', $specie), 'method' => 'put'])}}
<p>{{ Form::label('name', 'Nom :') }} {{ Form::text('name') }}</p>
<p>{{ Form::label('classification', 'Classification :') }} {{ Form::text('classification') }}</p>
{{ Form::submit() }}
{{ Form::close() }}
