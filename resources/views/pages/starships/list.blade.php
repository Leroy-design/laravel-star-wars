

<h1>Les vaisseaux interstellaires présents </h1>
<div class="container">

    <div class="row pt-5">
        <div class="col-4 pb-4">
            <table>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Longueur</th>
                    <th>Cote de l'hyperdrive</th>
                    <th>Pilote</th>
                </tr>
                @foreach($starships as $starship)

                    <tr>
                        <th>{{$starship->id}}</th>
                        <th>{{$starship->name}}</th>
                        <th>{{$starship->length}}</th>
                        <th>{{$starship->hyperdrive_rating}}</th>
                        <th>{{$starship->persons->pluck('name')}}</th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
