<h1>Création d'un vaisseau interstellaire</h1>

{{ Form::open([
    'route' => 'starship.store',
    'method' => 'POST'
])}}
    <div>
        {{ Form::label('name', 'Nom :')}}
        {{ Form::text('name') }}
    </div>
    <div>
        {{ Form::label('length', 'Longueur : ') }}
        {{ Form::text('length') }}
    </div>
    <div>
        {{ Form::label('hyperdrive_rating', "Cote de l'hyperdrive : ") }}
        {{ Form::text('hyperdrive_rating') }}
    </div>
{{ Form::submit() }}
