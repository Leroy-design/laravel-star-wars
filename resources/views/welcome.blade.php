<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Star Wars</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }


        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        body {
            background-image: url('https://ak9.picdn.net/shutterstock/videos/22001749/thumb/1.jpg');
            background-repeat: repeat;
            background-size: auto;
            position: relative;
            height: 100vh;
            overflow: hidden;
        }

        audio {
            position: absolute;
        }

        @font-face {
            font-family: Starjedi;
            src: url('fonts/Starjedi.ttf');
        }

        @font-face {
            font-family: Starjout;
            src: url('fonts/Starjout.ttf');
        }

        @keyframes title-shake {
            0% {
                transform: translateY(-5px);
            }
            25% {
                transform: translateY(-10px);
            }
            50% {
                transform: translateY(10px);
            }
            75% {
                transform: translateY(-10px);
            }
            100% {
                transform: translateY(10px);
            }
        }

        .spaceship-1, .spaceship-2 {
            height: 150px;
            width: 300px;
            background-size: contain;
            background-repeat: no-repeat;
            position: absolute;
            top: -300px;
            left: -5em;
            animation: spaceship1-move linear infinite 4s;
        }

        .spaceship-1 {
            background-image: url("img/Xwing.png");
            animation: spaceship1-move linear infinite 4s;
        }

        .spaceship-2 {
            background-image: url("img/tie.png");
            animation-delay: 3.3s;
            animation: spaceship2-move linear infinite 4s;
        }

        @keyframes spaceship1-move {
            0% {
                top: -3em;
                left: -5em;
            }
            25% {
                top: 5em;
                left: 10em;
            }
            50% {
                top: 10em;
                left: 15em;
            }
            75% {
                top: 20em;
                left: 20em;
            }
            100% {
                top: 110%;
                left: 110%;
            }
            100% {
                top: 110%;
                left: 110%;
            }
        }

        @keyframes spaceship2-move {
            0% {
                top: -1em;
                left: -2em;
            }
            25% {
                top: 2em;
                left: 3em;
            }
            50% {
                top: 10em;
                left: 15em;
            }
            75% {
                top: 30em;
                left: 50em;
            }
            100% {
                top: 110%;
                left: 110%;
            }
            100% {
                top: 110%;
                left: 110%;
            }
        }


        .title {
            font-size: 80px;
            color: white;
            /*color: black;*/
            margin-top: 300px;
            margin-bottom: 2em;
            font-family: "Starjout", sans-serif;
            animation: title-shake alternate-reverse infinite 3s;
        }

        .navbar-dark .navbar-nav .nav-link {
            color: white !important;
            opacity: 1 !important;
            font-size: 2em;
            margin: 0 1em;
        }

        .flex {
            display: flex;
        }

        @keyframes spaceship-millenium {
            0% {
                transform: translateY(-5px) translateX(-130em);
            }
            25% {
                transform: translateY(-30px) translateX(-5px) ;
            }
            50% {
                transform: translateY(10px);
            }
            75% {
                transform: translateY(-10px);
            }
        }
        @keyframes spaceship-shake {
            0% {
                transform: translateY(-5px);
            }
            25% {
                transform: translateY(-30px);
            }
            50% {
                transform: translateY(10px);
            }
            75% {
                transform: translateY(-10px);
            }
        }

        .spaceship-commander {
            height: 200px;
            width: 500px;
            background-image: url("img/destroyer.png");
            background-size: contain;
            background-repeat: no-repeat;
            position: absolute;
            bottom: 0.5em;
            left: 50em;
            animation-name: spaceship-millenium, spaceship-shake;
            animation-duration: 3s;
            animation-iteration-count: 1, infinite;
        }
        .pause-unpause {
            width: 8em;
            position: absolute;
            right: 1em;
            top: 1em;
            background-color: red;
            border: none;
            z-index: 999999;
        }

    </style>
</head>
<body>
<button class="pause-unpause">Mute Sound</button>
<audio id="pls" autoplay="autoplay" controls="controls" class="player-audio bg-sound" >
    <source src="audio/star-wars-bg-song.mp3" type="audio/mpeg">

    Your browser does not support the audio element.
</audio>
<audio id="pls" autoplay="autoplay" controls="controls" class="player-audio" loop>
    <source src="audio/tie-moving.mp3" type="audio/mpeg">
</audio>
<audio id="pls" autoplay="autoplay" controls="controls" class="player-audio laser-blast" loop>
    <source src="audio/laser-blast.mp3" type="audio/mpeg">
</audio>
<span class="spaceship-1"></span>
<span class="spaceship-2"></span>
<span class="spaceship-commander"></span>
<div class="flex position-ref full-height justify-content-center">
    <div class="content">
        <div class="title bounce infinite">
            The one and only Star Wars Project
        </div>
        <nav class="navbar navbar-expand-lg navbar-light" style="color: white">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse navbar-dark" id="navbarSupportedContent">
                <ul class="navbar-nav navbar-center">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPersons" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Personnages
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownPersons">
                            <a class="dropdown-item" href="{{ route('person.index') }}">Liste des personnages</a>
                            <a class="dropdown-item" href="{{ route('person.create') }}">Création</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPersons" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Vehicules
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownPersons">
                            <a class="dropdown-item" href="{{ route('person.index') }}">Liste des personnages</a>
                            <a class="dropdown-item" href="{{ route('person.create') }}">Création</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPersons" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Planètes
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownPersons">
                            <a class="dropdown-item" href="{{ route('person.index') }}">Liste des personnages</a>
                            <a class="dropdown-item" href="{{ route('person.create') }}">Création</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPersons" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Vaisseaux
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownPersons">
                            <a class="dropdown-item" href="{{ route('person.index') }}">Liste des personnages</a>
                            <a class="dropdown-item" href="{{ route('person.create') }}">Création</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPersons" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Espèces
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownPersons">
                            <a class="dropdown-item" href="{{ route('person.index') }}">Liste des personnages</a>
                            <a class="dropdown-item" href="{{ route('person.create') }}">Création</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="alert('Je sens de la noirceur en toi')">Découvre la
                            force</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<script>

</script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script src="js/animation.js"></script>

</body>
</html>
