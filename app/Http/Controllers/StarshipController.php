<?php

namespace App\Http\Controllers;

use App\Models\Starship;
use Illuminate\Http\Request;

class StarshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $starships = Starship::with('persons')->get();
        return view('pages/starships/list', compact('starships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request )
    {
        return view('pages/starships/create');
    }

    public function edit($id)
    {
        $starship = Starship::findOrFail($id);
        return view('pages/starships/edit', compact('starship'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Starship::create([
            'name' => $request->input('name'),
            'length' => $request->input('length'),
            'hyperdrive_rating' => $request->input('hyperdrive_rating')
        ]);

        return view('pages/starships/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Starship::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $starship = Starship::findOrfail($id);

        $starship->update([
            'name' => $request->input('name'),
            'length' => $request->input('length'),
            'hyperdrive_rating' => $request->input('hyperdrive_rating')
        ]);

        return view('pages/starships/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Starship::destroy($id);
    }
}
