<?php

namespace App\Http\Controllers;

use App\Models\Starship;
use App\Models\Person;
use App\Models\Planet;
use App\Models\Specie;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vehicles = Vehicle::get()->mapWithKeys(function($vehicle) {
            return [$vehicle->id => $vehicle->name];
        });

        $starships = Starship::get()->mapWithKeys(function($vehicle) {
            return [$vehicle->id => $vehicle->name];
        });

        $planets = Planet::get()->mapWithKeys(function($planet) {
            return [$planet->id => $planet->name];
        });

        $species = Specie::get()->mapWithKeys(function($specie) {
            return [$specie->id => $specie->name];
        });

        $search = $request->input('search');
        $planet = $request->input('planet');
        $specie = $request->input('specie');
        $selectedStarships = $request->input('starships');
        $selectedVehicles = $request->input('vehicles');

        $query = Person::with(['planet', 'specie', 'vehicles', 'starships']);
        
        if ($search != null) {
            $query->where('name', 'LIKE', '%' . $search . '%')
                ->orWhere('id', 'LIKE', '%' . $search . '%')
                ->orWhere('height', 'LIKE', '%' . $search . '%')
                ->orWhere('masse', 'LIKE', '%' . $search . '%')
                ->orWhere('gender', 'LIKE', '%' . $search . '%');
            }

        if ($planet) {
            $query->where('planet_id', $planet);
        }

        if ($specie) {
            $query->where('specie_id', $specie);
        }

        if ($selectedVehicles) {
            $query->whereHas('vehicles', function($query) use ($selectedVehicles) {
                $query->whereIn('vehicle_id', $selectedVehicles);
            });
        }

        if ($selectedStarships) {
            $query->whereHas('starships', function($query) use ($selectedStarships) {
                $query->whereIn('starship_id', $selectedStarships);
            });
        }


        $persons = $query->get();

        return view('pages/persons/list', compact(
            'persons',
            'search',
            'vehicles',
            'selectedVehicles',
            'starships',
            'selectedStarships',
            'planets',
            'planet',
            'species',
            'specie'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $person = Person::create([
            'name' => $request->input('name'),
            'height' => $request->input('height'),
            'masse' => $request->input('masse'),
            'gender' => $request->input('gender'),
            'planet_id' => $request->input('planet'),
            'specie_id' => $request->input('specie')
        ]);

        if ($request->input('vehicles')) {
            $person->vehicles()->sync($request->input('vehicles'));
        }

        if ($request->input('starships')) {
            $person->starships()->sync($request->input('starships'));
        }

        return view('pages/persons/list');
    }

    public function create()
    {
        $vehicles = Vehicle::get()->mapWithKeys(function($vehicle) {
            return [$vehicle->id => $vehicle->name];
        });

        $starships = Starship::get()->mapWithKeys(function($vehicle) {
            return [$vehicle->id => $vehicle->name];
        });

        $planets = Planet::get()->mapWithKeys(function($planet) {
            return [$planet->id => $planet->name];
        });

        $species = Specie::get()->mapWithKeys(function($specie) {
            return [$specie->id => $specie->name];
        });

        return view('pages/persons/create', compact('vehicles', 'planets', 'species', 'starships'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Person::findOrFail($id);
    }

    public function edit($id)
    {
        $vehicles = Vehicle::all()->mapWithKeys(function($vehicle) {
            return [$vehicle->id => $vehicle->name];
        });

        $starships = Starship::all()->mapWithKeys(function($vehicle) {
            return [$vehicle->id => $vehicle->name];
        });

        $planets = Planet::all()->mapWithKeys(function($planet) {
            return [$planet->id => $planet->name];
        });

        $species = Specie::all()->mapWithKeys(function($specie) {
            return [$specie->id => $specie->name];
        });

        $person = Person::with(['planet', 'specie', 'vehicles', 'starships'])->where('id', $id)->first();

        return view('pages/persons/edit', compact(
            'person',
            'vehicles',
            'starships',
            'planets',
            'species',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $person = Person::findOrfail($id);

        $person->update([
            'name' => $request->input('name'),
            'height' => $request->input('height'),
            'masse' => $request->input('masse'),
            'gender' => $request->input('gender'),
            'planet_id' => $request->input('planet'),
            'specie_id' => $request->input('specie')
        ]);

        if ($request->input('vehicles')) {
            $person->vehicles()->sync($request->input('vehicles'));
        }

        if ($request->input('starships')) {
            $person->starships()->sync($request->input('starships'));
        }

        return view('pages/persons/list');
    }

    public function search(Request $request)
    {
        dd('test');
        $query = Person::where('name', 'like', $request->input('search'));
        return view('pages/persons/list', compact('persons'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return int
     */
    public function destroy($id)
    {
        return Person::destroy($id);
    }
}
