<?php

namespace App\Http\Controllers;

use App\Models\Planet;
use Illuminate\Http\Request;

class PlanetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planets= Planet::with('persons')->get();
        return view('pages/planets/list', compact('planets'));
    }

    public function create()
    {
        return view('pages/planets/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Planet::create([
            'name' => $request->input('name'),
            'diameter' => $request->input('diameter'),
            'climate' => $request->input('climate'),
            'population'=> $request->input('population'),
        ]);

        return view('pages/planets/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  Planet::findOrFail($id);
    }

    public function edit($id)
    {
        $planet = Planet::findOrFail($id);

        return view('pages/planets/edit',compact('planet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $planet = Planet::findOrFail($id);

        $planet->update([
            'name' => $request->input('name'),
            'diameter' => $request->input('diameter'),
            'climate' => $request->input('climate'),
            'population'=> $request->input('population'),
        ]);

        return view('pages/planets/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
