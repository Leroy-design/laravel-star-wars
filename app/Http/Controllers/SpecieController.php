<?php

namespace App\Http\Controllers;

use App\Models\Specie;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SpecieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Specie[]
     */
    public function index()
    {
        $species = Specie::with('persons')->get();
        return view('pages/species/list', compact('species'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Specie
     */
    public function store(Request $request)
    {
        Specie::create([
            'name' => $request->input('name'),
            'classification' => $request->input('classification')
        ]);

        return view('pages/species/list');
    }

    /**
     * @return View
     */
    public function create()
    {
        return view('pages/species/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Specie::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Specie::where('id', $id)
            ->update([
                'name' => $request->input('name'),
                'classification' => $request->input('classification'),
            ]);
        
        return view('pages/species/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Specie::destroy($id);
    }
}
