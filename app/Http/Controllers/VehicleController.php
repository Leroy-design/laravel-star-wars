<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\View\View;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Vehicle[]
     */
    public function index()
    {
        $vehicles = Vehicle::with('persons')->get();
        return view('pages/vehicles/list', compact('vehicles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Vehicle
     */
    public function store(Request $request)
    {
        Vehicle::create([
            'name' => $request->input('name'),
            'length' => $request->input('length')
        ]);

        return view('pages/vehicles/list');
    }

    /**
     * @return View
     */
    public function create()
    {
        return view('pages/vehicles/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Vehicle::findOrFail($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function edit($id)
    {
        $vehicle = Vehicle::findOrFail($id);

        return view('pages/vehicles/edit', compact('vehicle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vehicle = Vehicle::findOrFail($id);

        $vehicle->update([
            'name' => $request->input('name'),
            'length' => $request->input('length'),
        ]);

        return view('pages/vehicles/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Vehicle::destroy($id);
    }
}
