<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'persons';
    
    protected $fillable = [
        'name',
        'height',
        'masse',
        'gender',
        'planet_id',
        'specie_id',
    ];

    public function vehicles()
    {
        return $this->belongsToMany('App\Models\Vehicle');
    }

    public function starships()
    {
        return $this->belongsToMany('App\Models\Starship');
    }

    public function specie()
    {
        return $this->belongsTo('App\Models\Specie');
    }

    public function planet()
    {
        return $this->belongsTo('App\Models\Planet');
    }

    public function scopeLower($query)
    {
        return $query->where('votes', '>', 100);
    }
}
