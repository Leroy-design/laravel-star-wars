<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('vehicle', 'VehicleController');

Route::resource('person', 'PersonController');
Route::get('person/search', 'PersonController@search')->name('person.search');

Route::resource('specie', 'SpecieController');

Route::resource('planet', "PlanetController");

Route::resource('starship', "StarshipController");


