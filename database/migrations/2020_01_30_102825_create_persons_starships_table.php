<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonsStarshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_starship', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->integer('person_id')->unsigned();
            $table->integer('starship_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('persons');
            $table->foreign('starship_id')->references('id')->on('starships');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons_starships');
    }
}
