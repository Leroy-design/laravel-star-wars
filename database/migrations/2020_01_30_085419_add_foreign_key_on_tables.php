<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyOnTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->integer('specie_id')->unsigned();
            $table->foreign('specie_id')->references('id')->on('specie');
        });

        Schema::table('persons', function (Blueprint $table) {
            $table->integer('planet_id')->unsigned();
            $table->foreign('planet_id')->references('id')->on('specie');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
