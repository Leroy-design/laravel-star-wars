<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonsVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_vehicle', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->integer('person_id')->unsigned();
            $table->integer('vehicle_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('persons');
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons_vehicles');
    }
}
